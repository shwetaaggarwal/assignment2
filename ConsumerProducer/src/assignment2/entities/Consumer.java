package assignment2.entities;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Queue;

public class Consumer implements Runnable{

	private final Queue<Integer> productList;

	public Consumer(Queue<Integer> productList) {

		this.productList = productList;
	}

	@Override
	public void run() {

		while (true) {
			try {

				System.out.println("Consumed: " + consume());
				Thread.sleep(3000);

			}

			catch (Exception ex) {
				System.out.println(ex);
			}
		}

	}

	private int consume() throws Exception{
		
        while (productList.isEmpty()) {
            synchronized (productList) {
                System.out.println("Queue is empty " + Thread.currentThread().getName()
                                    + " is waiting , size: " + productList.size());

                productList.wait();
            }
        }

       
        synchronized (productList) {
            productList.notifyAll();
            return (Integer) productList.remove();
        }
		
		
	
	}

	

}

package assignment2.entities;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Queue;

public class Producer implements Runnable {

	private final Queue<Integer> productList;

	public Producer(Queue<Integer> productList) {
		this.productList = productList;
	}

	@Override
	public void run() {
		int i = 1;
		while (true) {

			System.out.println("Produced: " + i);
			try {

				produce(i);
				i++;
			}

			catch (Exception ex) {
				System.out.println(ex);
			}
		}
	}

	private void produce(int i) throws Exception {

		while (productList.size() == 20) {
			synchronized (productList) {
				System.out.println("Queue is full "
						+ Thread.currentThread().getName()
						+ " is waiting , size: " + productList.size());

				productList.wait();
			}
		}

		synchronized (productList) {
			productList.add(i);
			productList.notifyAll();
		}

	}

}

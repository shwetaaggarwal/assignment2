package assignment2.controller;


import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.security.KeyException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

import assignment2.entities.Consumer;
import assignment2.entities.Producer;


 

public class Controller implements KeyListener{

static	Scanner scan=new Scanner(System.in);
static	Queue<Integer> productList =new LinkedList<Integer>();
	
static	Thread prodThread = new Thread(new Producer(productList));
 static   Thread consThread = new Thread(new Consumer(productList));

public static void main(String args[]) {
	
	
	
	
   
    prodThread.start();
    consThread.start();
	
    
scan.nextLine();
prodThread.stop();
consThread.stop();
	

    
	
}

@Override
public void keyTyped(KeyEvent e) {
	
	System.exit(0);
	
}

@Override
public void keyPressed(KeyEvent e) {

	System.exit(0);
	
}

@Override
public void keyReleased(KeyEvent e) {
	
	System.exit(0);
	
}


}
